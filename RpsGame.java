// Sophia Marshment 2038832

import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    private Random rand;

    public RpsGame() {
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;

        rand = new Random();
    }

    public int getWins() {
        return this.wins;
    }

    public int getTies() {
        return this.ties;
    }

    public int getLosses() {
        return this.losses;
    }


    public String playRound(String playerSymbol) {
        String result = "";
        int randNum = rand.nextInt(3);
        String computerSymbol;

        if (randNum == 0) {computerSymbol = "rock";}
        else if (randNum == 1) {computerSymbol = "scissors";}
        else {computerSymbol = "paper";}

        if (playerSymbol.equals(computerSymbol)) {this.ties++; result = "tie";}
        else if (playerSymbol.equals("rock")) {
            if (computerSymbol.equals("scissors")) {this.wins++; result = "win";}
            else {this.losses++; result = "loss";}
        }
        else if (playerSymbol.equals("scissors")) {
            if (computerSymbol.equals("paper")) {this.wins++; result = "win";}
            else {this.losses++; result = "loss";}
        }
        else if (playerSymbol.equals("paper")) {
            if (computerSymbol.equals("rock")) {this.wins++; result = "win";}
            else {this.losses++; result = "loss";}
        }

        if (result.equals("tie")) {result = "Tie! Computer played "+computerSymbol+".";}
        else if (result.equals("win")) {result = "You win! Computer played "+computerSymbol+".";}
        else {result = "You lose! Computer played "+computerSymbol+".";}

        return result;
    }
}
