// Sophia Marshment 2038832

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {
    private RpsGame game;

	public void start(Stage stage) {
        game = new RpsGame();
		Group root = new Group();

        VBox vb = new VBox();
        HBox buttons = new HBox();
        HBox textfields = new HBox();
        Button b1 = new Button("rock");
        Button b2 = new Button("scissors");
        Button b3 = new Button("paper");
        buttons.getChildren().addAll(b1, b2, b3);
        TextField tf1 = new TextField("Welcome!");
        tf1.setPrefWidth(200);
        TextField tf2 = new TextField("wins: 0");
        TextField tf3 = new TextField("losses: 0");
        TextField tf4 = new TextField("ties: 0");
        textfields.getChildren().addAll(tf1, tf2, tf3, tf4);
        vb.getChildren().addAll(buttons, textfields);
        root.getChildren().add(vb);

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 

        b1.setOnAction(new RpsChoice(tf1, tf2, tf3, tf4, "rock", game));
        b2.setOnAction(new RpsChoice(tf1, tf2, tf3, tf4, "scissors", game));
        b3.setOnAction(new RpsChoice(tf1, tf2, tf3, tf4, "paper", game));
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}
