// Sophia Marshment 2038832

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

public class RpsChoice implements EventHandler<ActionEvent>{
    private TextField message;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private String playerChoice;
    private RpsGame game;


    public RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, String playerChoice, RpsGame game) {
        this.message = message;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.playerChoice = playerChoice;
        this.game = game;
    }
    
    public void handle(ActionEvent e) {
        String result = game.playRound(playerChoice);

        this.message.setText(result);
        this.wins.setText("wins: "+Integer.toString(game.getWins()));
        this.losses.setText("losses: "+Integer.toString(game.getLosses()));
        this.ties.setText("ties: "+Integer.toString(game.getTies()));  
    }
}
